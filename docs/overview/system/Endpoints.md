# Endpoints

###### Written by BindTuning
###### 27 Dec 2021
###### Applies to: Production, Partners

___

Automate365 applications communicate with the following endpoints, please make sure your firewalls and content filters allow access to the URLs in the following table. EasyLife 365 communicates exclusively over https (tcp/443).


| Endpoint                              | Protocol      | Comment                   |
| :---                                  | :----:        | ---:                      |
| app.bindtuning.com                    | https         | BindTuning App            |
| a365-provisioning                     | https         | A365 Provisioning Engine  |
| a365-authentication                   | https         | A365 API                  |
| a365-datadriven                       | https         | A365 Data Driven          |