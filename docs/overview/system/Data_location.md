# Data location

###### Written by BindTuning
###### 27 Dec 2021
###### Applies to: Production, Partners

___

Automate365 aims to reduce the footprint of customer information stored in their environments. The Azure Table Storage only contains configuration information such as templates and policies needed by Automate365 to take decisions for the lifecycle or your resources. By default, this information is stored in our European data centers. American and Asia Pacific customers can have their configuration data stored in America and Asia Pacific if required.

Automate365 tracks operations executed by the Automate365 engine for internal support and troubleshooting purposes for a period of 14 days. Automate365 stores information such as Microsoft 365 Group Metadata (e.g., Title, Description) or user contact data (e.g. e-mail address, preferred language) for this period of time.

All security keys required to access your tenant and other Automate365 resources are stored in Azure Key Vaults. Where possible, security keys have been avoided completely and replaced with Managed Identities associated to all Azure resources using role-based access control.

All other information is directly residing in your Microsoft 365 environment and data location.

- Microsoft 365 Group status is tracked using Microsoft 365 open extension properties associated to your resource. These open extensions are registered to Automate365 and are not affecting other processes or data in your environment
- All operations performed by Automate365 App except the creation of new resources is executed in the context of the end-user performing the operation. All activities are tracked in your Microsoft Audit Logs
- All background operations performed by the Automate365 Engine (e.g. creation of resources, applying changes to your resources based on your policy settings) are tracked in your Microsoft Audit Logs and associated to the Azure AD Application registered for Automate365