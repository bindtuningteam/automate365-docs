# System Overview

###### Written by BindTuning
###### 27 Dec 2021
###### Applies to: Production, Partners

___

## App
The **Automate365** is an Azure Web app responsible to host the end-user facing application. It is accessible through a web browser and directly through <a href="https://app.bindtuning.com/automte365" target="_blank">BindTuning App</a>. The authentication is ensured using a dedicated Azure AD application in the Microsoft Identity platform and can therefore be secured using Microsoft security standards. The aim of the application is to provide an overview of the resources owned by a user with all the necessary compliancy requirements.


## API
The Automate365 API is accessed by the <a href="https://app.bindtuning.com" target="_blank">BindTuning App</a> and to manage the necessary information stored in the back-end storage. This Web App is secured through a dedicated Azure Active Directory app using the Microsoft Identity Platform.

The Automate365 API uses Microsoft Graph to interact with the Microsoft 365 environment. The access to the endpoints is secured by using custom security scopes associated to your **Automate365** apps and users.


## Engine
The Automate365 Provisioning Engine is an Azure Function that is responsible to provision new resources and performing regular compliancy checks in your tenant. It is also responsible to send notifications to your users and administrators - notification emails and it can also send notifications through Microsoft Teams.

The engine can also send notifications to any other application or service using the Webhook feature.

The Automate365 Data Driven is a separate process and can be enabled or disabled. The engine’s operations are executed in the background using a service of featured Webhooks.


## Storage
The **Automate365** configuration (e.g. templates) are being stored in Azure Table Storages and Blobs. The Storage account is accessible by the Automate365 API, Automate365 Provisioning Engine and BindTuning App.


## Logging
Application insights is used to log the operations performed by **Automate365**. It will maintain 14 days of logs containing information about the metadata of Groups processed and e-mails of the users receiving notifications.


## Microsoft Graph
<a href="https://docs.microsoft.com/en-us/graph/overview" target="_blank">Microsoft Graph</a> is used by the **Automate365** components to interact with the Microsoft 365 tenant. Microsoft Graph is the data gateway to data and intelligence in Microsoft 365. It provides a unified programmability model that you can use to access the tremendous amount of data in Microsoft 365, Windows 10, and Enterprise Mobility + Security.

## PnP Framework 
The <a href="https://github.com/pnp/pnpframework" target="_blank">PnP provisioning framework</a> provides a code-centric and template-based platform for provisioning your site collections. The new provisioning engine allows you to persist and reuse provisioning models in Office 365 and SharePoint Online site collections.


## Azure Active Directory
The Microsoft Identity Platform is used in combination with Azure Active Directory to secure the access to all **Automate365** components. The BindTuning App, Automate365 API, and Automate365 Provisionign Engine have dedicated Azure Active Directory app registrations that can be secured using techniques such as Conditional Access.