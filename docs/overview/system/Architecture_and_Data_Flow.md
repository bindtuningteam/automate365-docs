# Architecture and Data Flow

###### Written by BindTuning
###### 27 Dec 2021
###### Applies to: Production, Partners

___

This page gives you an overview how the Automate365 SaaS components interact with each other and how the environment is accessed. PaaS customer can follow the same architecture, or a different setup based on their requirements.

![Automate365 Architecture](/images/BindTuning365 Architecture.png)

All components are secured behind an Azure Firewall blocking all external internet traffic to the BindTuning environment. Only few services such as our Azure Dev Ops for the automated deployment and selected engineers over a secured network for emergency purposes have access to the environment.

Incoming user traffic is routed through an Azure Front Door and Web Application Firewall. This component is used for load balancing and secures the web applications and the environment from documented vulnerabilities. See details <a href="https://docs.microsoft.com/en-us/azure/web-application-firewall/ag/ag-overview" target="_blank">here</a>. All endpoints are secured using Azure Active Directory applications using the Microsoft Identity Platform. All interactions between the internal applications are secured with role-based access control and managed identities. Azure Key vaults, accessible only by managed identities and selected security engineers at BindTuning, store the security keys when a managed identity cannot be used for authentication purposes.

The BindTuning App and Automate365 use Microsoft 365 Graph using delegated identity permissions to perform activities on your Microsoft 365 tenant. This means that users are only able to perform operations that they can perform in your Microsoft 365 tenant. These are examples of operations executed by these apps:

- Read items in all site collections
- Read and write to all app catalogs
- Read and write all groups
- Read the membership of a group

Users can request in the BindTuning App the creation of new templates. All operations performed on these configurations are passing through the Automate365 API. The BindTuning 365 App reads the information from the configuration while the Automate365 allows you to create, update and delete the template collected based on your requirements.

The Automate365 Provisioning Engine handles new resource requests coming from users. The creation of new templates is performed with Microsoft Graph and PnP Framework. All operations are executed in the context of the user logged in the application. The Automate365 Provisioning Engine is also verifying the compliance of Microsoft 365 Groups by using Microsoft Graph. It performs following operations:

- It retrieves all Microsoft Unified Groups in your environment
- It verifies the open extensions associated to the Microsoft Group, based on the properties stored there, it takes the decisions based on the configuration specified for your tenant
- It verifies the owners of a Group
- It triggers a workflow and tracks the status of the workflow using the open extensions associated to your Microsoft 365 Group
- It deletes or archives a Group based on the configuration specified for your tenant

The Automate365 environment stores the information in multiple storage locations and accounts to ensure resiliency and improve performance for its customers. The data partition is ensured with the TenantID of the customer. The correct TenantID information applied to the different web accessible endpoints is ensured by the Microsoft Identity Platform. Microsoft Graph is using the generated access tokens to ensure the right access to proper resources by using the security tokens provided by the Microsoft Identity Platform.