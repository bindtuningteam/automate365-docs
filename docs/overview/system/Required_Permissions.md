# Required Permissions

###### Written by BindTuning
###### 27 Dec 2021
###### Applies to: Production, Partners

___

Automate365 uses the Microsoft Identity Platform to manage the authentication and authorization layer against your Microsoft 365 tenant. It uses the Microsoft Graph to access your resources.


## Automate365 Provisioning Engine
The Automate365 Provisioning Engine executes operations in a delegated scope. An app with the delegated permission acts as the signed-in user when making calls to the target resource. The Microsoft Graph ensures that operations executed in this context are executed at the permission level of to signed-in user. This application can…

- Sign in the current user using the Microsoft Identity Platform
- Create, change, and delete Microsoft 365 Groups in the name of the user
- Read the users profiles of your organization
- Call the secured administrative endpoint in the Automate365 API
- Send activity notifications to users in Teams
- Update itself


## BindTuning App
The BindTuning App executes operations in a delegated scope. An app with the delegated permission acts as the signed-in user when making calls to the target resource. The Microsoft Graph ensures that operations executed in this context are executed at the permission level of to signed-in user. This application can…

- Sign in the current user using the Microsoft Identity Platform
- Create, change, and delete Microsoft 365 Groups in the name of the user
- Read the users profiles of your organization
- Call the end user facing endpoints in the Automet365 API


## Automate365 API
The Automate365 API is used by the Bindtuning App and Automate365 API to retrieve configuration data. These endpoints are secured using the Microsoft Identity Platform. The Automate365 API is also used by the BindTuning App, which is performing all the heavy-lifting in the background, enforcing the life cycle of the Microsoft 365 resources that are managed through Automate365. The application acts as a daemon app that requires permissions to your tenant to provision, modify, and delete resources and to execute the required life cycle operations. To do so, this application requires permissions to…

- Manage groups and directory data
- Manage items in your site collections
- Read all users in your organization
- Read audit log data
- Read usage reports
- Read Mailbox settings
- Send emails
- Send activity notifications to users in Teams
- Update itself

The identities and credentials used to perform the operations are securely stored in Azure Key Vaults in the EasyLife 365 infrastructure. Since all applications are managed in the Microsoft Identity Platform, all customers have the possibility to restrict the access to apps by using conditional access rules or access configurations available in the enterprise application configuration of Azure AD.


## Permission details
A full list of required permissions is available <a href="https://support.bindtuning.com/hc/en-us/articles/360052818511-Why-do-I-need-to-accept-BindTuning-permissions-request-to-install-products-" target="_blank">here</a>