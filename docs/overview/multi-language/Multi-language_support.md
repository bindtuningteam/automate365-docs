# Endpoints

###### Written by BindTuning
###### 27 Dec 2021
###### Applies to: Production, Partners

___

Automate365 supports English, German, Italian, French, and Spanish as default languages for the BindTuning App. Contact your BindTuning representative if you must introduce a new language in the Automate365 ecosystem.

Automate365 always considers the language in the language settings. We ignore the region-specific configuration. For example, en-us (English United States) and en-uk (English United Kingdom) will be treated as English version in EasyLife 365.

Whenever an Automate365 components must decide which language to choose, it will try to find a translation in the specified language. If nothing is found, it will fallback to the default language (English).


## BindTuning App
The BindTuning App take the browser language when opened as standalone version. The same is valid when embedding the EasyLife 365 App in Microsoft Teams. It will take by default the system language. The language can also be overridden by the language settings in the Microsoft Teams client using the App language.


## Automate365 templates
The Automate365 templates shown to your users in the Wizard including their title, description, and metadata can also be translated in multiple languages. You can use the language bar settings in the corresponding section.


## Automate365 Data Driven mail notifications
Automate365 Data Driven notification emails are sent by the BindTuning App. The engine will send notification emails **only** in English