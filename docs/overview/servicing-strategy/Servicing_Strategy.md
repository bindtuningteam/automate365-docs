# Endpoints

###### Written by BindTuning
###### 27 Dec 2021
###### Applies to: Production, Partners

___

All incoming feature requests are collected and internally aligned with our BindTuning team. You can submit a feature request using the <a href="https://bindtuning.uservoice.com/forums/10329-my-bindtuning-idea" target="_blank">User Voice portal</a>. After verification, based on the type of request, the feature is put on the Automate365 road-map. The aim of the road-map is to give customers and partners an expected time frame for delivery of upcoming features of Automate365. You can find the features we are currently working on here.

In some situations requested features are not going to make it into the product. In such situations, the customer can use one of our extension capabilities, such as webhooks, or get in touch with our integrations team to find a dedicated solution to their problem.


## Deployment rings
Automate365 development follows a (roughly) monthly update cycle for most features. When a new feature is ready our devs push it to the internal Automate365 Staging ring first. There we run a series of automated tests and perform manual validation steps. If both are successful, new features are pushed from the internal staging ring to the Insider ring in the next update cycle. All customers and partners can use the Insider ring in their test environments for no additional cost. This allows you to get to know the new features before they are pushed to the production environment. We perform additional validation in the insider deployment ring and customers and partner can reach out to us if they encounter a bug.

Also in every update cycle, the current insider ring is pushed to production where all partners and customers access it on a daily basis.


## Hotfixes
Changes requiring immediate attention will be shipped as hotfixes after internal testing. These hotfixes will be pushed to the corresponding Automate365 production instances.