# Automate365 101

###### Written by BindTuning
###### 27 Dec 2021
###### Applies to: Production, Partners

___

**Automate365** uses the Microsoft Identity Platform to manage the authorization and authentication layer against your Microsoft 365 tenant. Your Microsoft 365 administrator will consent permissions to either the multi-tenant application identities managed in the **Automate365** SaaS or to a dedicated application identity registered in your tenant. Once the permissions to your tenant have been consented you can start provisioning products over the <a href="https://app.bindtuning.com" target="_blank">BindTuning App</a>.

**Automate365** allows you to create templates and provisioning BindTuning products such Themes, Web Parts, Starter Kits, etc.
Templates are created from who in your organization has permissions to collect site collections or a team all collect resources and how these are configured during the provisioning process. For example, you can control how a Microsoft 365 Team is provisioned with its dedicated channels, tabs and tabs app configurations settings by configuring the templates based to your needs. A template defines all the aspects relevant to a resource such as:

- How is the template provisioned?
- What are the naming conventions when creating a new resource?
- What security features are enabled? For example: guest account access, Teams settings
- What type of resource is being provisioned? For example: Microsoft 365 Group, Team.
- What data is being collected from the users?
- What policy is being applied?
- Which users (audience) can consume this template?
- Is this template enabled for your users?
- What webhooks are triggered once a new resource is being provisioned?

The templates are being maintained in the section of the **Automate365** inside BindTuning App. The remaining resource life cycle is managed with the **Automate365** wizard installation. The installation wizzard  allow to control how the resources are managed with regards to ownership, configurations, explicit confirmation, and access review.

On top of that, you can also use the PRovisioning engine with a set of webooks.......