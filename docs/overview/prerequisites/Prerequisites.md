# Endpoints

###### Written by BindTuning
###### 27 Dec 2021
###### Applies to: Production, Partners

___

Users must be assigned a valid license for the products they want to mange with Automate365. Users must be assigned a license that includes Office365 / Microsoft Teams before they can collect and provision new Site Collection or/and Teams inside Microsoft Teams.

The onboarding process must have been completed before users can manage resources using the BindTuning App. This includes granting the required permissions to Automate365 in your tenant’s Azure AD. Check the onboarding section for more details.
