# Templates

###### Written by BindTuning
###### 28 Dec 2021
###### Applies to: Production, Partners

___

## Templates
Templates are organized in modules. Each module represents a structure from you Microsoft 365 enviroment (Office365 site collection and/or Microsoft Teams team). Automate365 supports the provisioning of Microsoft Groups (including group connected SharePoint sites), Teams, etc.

You can create new templates and click on each module to install or update template for the resources in the respective module.

A template defines all the aspects relevant to a resource such as:

- How is the resource provisioned?
- What are the naming conventions when creating a new resource?
- What security features are enabled?
- What type of resource is provisioned? (Office365 site collection or Teams)
- What data is collected?

![Tux, the Linux mascot](/assets/images/tux.png)


# Template overview
When you click on a module (eg. Office365 or Microsoft Teams) a wizard with an overview of all the existing contente will appear and you will be able to manage and select which options you intnt to include for the new template.