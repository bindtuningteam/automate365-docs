# Automate365

###### Written by BindTuning
###### 28 Dec 2021
###### Applies to: Production, Partners

___

The BindTuning App is your admin interface, it allows you to configure Automate365 experience and the behavior of its components in your organization. This web application is intended for IT Professionals and Service Managers in your organization. The access to this web application can be limited to assigned users with the correspondend BindTuning associated license.

We recommend you restrict access to the BindTuning App to a small group of administrators.

With the BindTuning App you can:

- Create templates
- Install products and templates
- Update templates
- Configure Automate365 Data Driven projects (comming soon)