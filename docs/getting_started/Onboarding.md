# Endpoints

###### Written by BindTuning
###### 28 Dec 2021
###### Applies to: Production, Partners

___

## Overview
The goal of the Automate365 onboarding process is to enable Automate365 in your organization. The activation is a self-service process performed by an IT administrator using the BindTuning App and Automate365 Data Driven.


## Prerequisites
- Tenant account to consent the permissions to Automate365 in your organization
- Global administrator account to consent the permissions to install products and collect templates in your organization
- Teams administrator account to consent the permissions to install products and collect templates in your organization


## Grant BindTuning App permissions to your tenant
Automate365 requires permissions to your tenant to perform its operations. Automate365 uses three Azure AD applications to accomplish this goal.

Automate365 API: used to provision and scan the resources in your Microsoft 365 tenant and ensure that these remain compliant. This application uses Microsoft Graph and PnP Framework to perform the heavy lifting in the background.

BindTuning App: this is the administrative aplication to use Automate365. It uses the Automate365 API and Automate365 Provisioning Engine to perform the required operations.